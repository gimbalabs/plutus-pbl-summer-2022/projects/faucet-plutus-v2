# Compiling with these parameters:

```
PPBLFaucetValidator.accessTokenSymbol   = "2021db9a8aa0c4fde51ce244b5ad8628a3045787eaf6fadc52f31ee9"
PPBLFaucetValidator.accessTokenName     = "PPBLSummer2022"
PPBLFaucetValidator.faucetTokenSymbol   = "381f0068861bb8916e28732215a981c5d14124445c5f2cc2b3a1ba22"
PPBLFaucetValidator.faucetTokenName     = "tgimbal"
PPBLFaucetValidator.withdrawalAmount    = 700
```

# Reference Transactions

## Send `PPBLSummer2022`
```
RECEIVER=
TXIN1=
TXIN2=
ACCESS_TOKEN="2021db9a8aa0c4fde51ce244b5ad8628a3045787eaf6fadc52f31ee9.5050424c53756d6d657232303232"
```

```
cardano-cli transaction build \
--babbage-era \
--testnet-magic 2 \
--tx-in $TXIN1 \
--tx-in $TXIN2 \
--tx-out $RECEIVER+"1500000 + 1 $ACCESS_TOKEN" \
--tx-out $SENDER+"1500000 + 149 $ACCESS_TOKEN" \
--change-address $SENDER \
--out-file draft.tx

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 2 \
--tx-body-file draft.tx \
--out-file signed.tx

cardano-cli transaction submit \
--tx-file signed.tx \
--testnet-magic 2
```

## Split UTxO
cardano-cli transaction build \
--babbage-era \
--testnet-magic 2 \
--tx-in $TXIN1 \
--tx-out $WALLET2+10000000 \
--tx-out $WALLET2+10000000 \
--tx-out $WALLET2+10000000 \
--tx-out $WALLET2+10000000 \
--change-address $WALLET2 \
--out-file draft.tx

cardano-cli transaction sign \
--signing-key-file $WALLET2KEY \
--testnet-magic 2 \
--tx-body-file draft.tx \
--out-file signed.tx

cardano-cli transaction submit \
--tx-file signed.tx \
--testnet-magic 2