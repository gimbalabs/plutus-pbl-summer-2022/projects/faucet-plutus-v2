# Faucet Plutus V2

## Everything You Need is On-Chain:
- Instead of passing around compiled Plutus Scripts, all you need is this Reference UTxO: `ec04bf0d3bd592716489fd470ba406c6aace5919cc6abeefa499228d37b6cbc5#1`
- Instead of wondering what the Datum is, you can query the Contract Address and look at it.
- You will still need a `PPBLSummer2022` token...this time on *Preview* Testnet. Share an address with James if you need one.
- You will still need to edit the `redeemer.json` file to include your `PubKeyHash`.

## Table of Contents
1. Compile Faucet Script
2. Build Address
3. Create Reference Script UTxO
4. Unlock Funds
5. Review Error Messages
6. Looking Ahead: When is a reference UTxO helpful for Datum?

## 1. Compile Faucet Script
- If you just want to use this Contract, the compilation steps are the same as usual.
- We will review how the Contract is different for PlutusV2.
- Review [Always Succeeds]() for help.

## 2. Build Address
As usual:
```
cardano-cli address build \
--payment-script-file ppbl-faucet-plutus-v2.plutus \
--testnet-magic 2 \
--out-file ppbl-faucet-plutus-v2.addr
```

For this example, the resulting address is: `addr_test1wzll697uka3jcgjk3fqc379v2scg3q894nmtcywjen4n7mcpauvup`

## 3. Create Reference Script UTxO
- We will follow the process outlined in the [Cardano Node documentation](https://github.com/input-output-hk/cardano-node/blob/master/doc/reference/plutus/babbage-script-example.md), simplified for clarity.
- Why is the MinUTxO so high for the Reference UTxO?

```
SENDER=
SENDERKEY=
TXIN=
TX_REF_ADDR=
CONTRACT_ADDR=addr_test1wzll697uka3jcgjk3fqc379v2scg3q894nmtcywjen4n7mcpauvup
PLUTUS_SCRIPT="<YOUR PATH TO>/faucet-plutus-v2/output/ppbl-faucet-plutus-v2.plutus"
FAUCET_ASSET="381f0068861bb8916e28732215a981c5d14124445c5f2cc2b3a1ba22.7467696d62616c"
```

```
cardano-cli transaction build \
--babbage-era \
--testnet-magic 2 \
--change-address $SENDER \
--tx-in $TXIN1 \
--tx-in $TXIN2 \
--tx-out $TX_REF_ADDR+16589190 \
--tx-out-reference-script-file $PLUTUS_SCRIPT \
--tx-out $CONTRACT_ADDR+"1500000 + 14000 $FAUCET_ASSET" \
--tx-out-inline-datum-value 1618 \
--tx-out $SENDER+"1500000 + 486000 $FAUCET_ASSET" \
--protocol-params-file preview-protocol.json \
--out-file tx.draft

cardano-cli transaction sign \
--tx-body-file tx.draft \
--testnet-magic 2 \
--signing-key-file $SENDERKEY \
--out-file tx.signed

cardano-cli transaction submit \
--testnet-magic 2 \
--tx-file tx.signed
```

## 4. Unlock Funds

#### Set Variables:

```
WALLET2_TX_IN_FEES=
WALLET2_TX_IN_ACCESS_TOKEN=
COLLATERAL_UTXO=
CONTRACT_UTXO=
REFERENCE_TX_IN=
REDEEMER_FILE="<YOUR PATH TO>/redeemer.json"
FAUCET_ASSET="381f0068861bb8916e28732215a981c5d14124445c5f2cc2b3a1ba22.7467696d62616c"
ACCESS_TOKEN="2021db9a8aa0c4fde51ce244b5ad8628a3045787eaf6fadc52f31ee9.5050424c53756d6d657232303232"

```

```
cardano-cli transaction build \
--babbage-era \
--testnet-magic 2 \
--tx-in $WALLET2_TX_IN_FEES \
--tx-in $WALLET2_TX_IN_ACCESS_TOKEN \
--tx-in-collateral $COLLATERAL_UTXO \
--tx-in $CONTRACT_UTXO \
--spending-tx-in-reference $REFERENCE_TX_IN \
--spending-plutus-script-v2 \
--spending-reference-tx-in-inline-datum-present \
--spending-reference-tx-in-redeemer-file $REDEEMER_FILE \
--tx-out $CONTRACT_ADDR+"1500000 + 13300 $FAUCET_ASSET" \
--tx-out $WALLET2+"1500000 + 1 $ACCESS_TOKEN" \
--tx-out $WALLET2+"1500000 + 700 $FAUCET_ASSET" \
--change-address $WALLET2 \
--protocol-params-file preview-protocol.json \
--out-file unlock-faucet-plutus-v2.draft

cardano-cli transaction sign \
--tx-body-file unlock-faucet-plutus-v2.draft \
--testnet-magic 2 \
--signing-key-file $WALLET2KEY \
--out-file unlock-faucet-plutus-v2.signed

cardano-cli transaction submit \
--testnet-magic 2 \
--tx-file unlock-faucet-plutus-v2.signed
```

## 5. Review Error Messages
Be sure to create some errors so that you can see that error messaging still works.
- Change the number `$FAUCET_ASSET`s to that the Validator will log `Must return remaining tokens to contract`
- Change the number `$FAUCET_ASSET`s to that the Validator will log `Faucet token must be distributed to sender`
- Leave the `$ACCESS_TOKEN` out of the transaction

## 6. Looking Ahead: When is a reference UTxO helpful for Datum?
- What are some reasons to use one here?
- What do you want to try next!?