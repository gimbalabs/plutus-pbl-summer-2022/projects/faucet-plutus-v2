{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module PPBLFaucetCompiler where

import              Prelude (FilePath, IO)
import              Cardano.Api
import              Cardano.Api.Shelley             ( PlutusScript (..), PlutusScriptV2 )
import              Codec.Serialise                 (serialise)
import              Data.Aeson
import qualified    Data.ByteString.Lazy            as LBS
import qualified    Data.ByteString.Short           as SBS
import qualified    Plutus.V1.Ledger.Scripts
import qualified    Plutus.V1.Ledger.Value
import qualified    Plutus.V2.Ledger.Api
import qualified    Plutus.V2.Ledger.Contexts
import qualified    PlutusTx
import              PlutusTx.Prelude


import qualified PPBLFaucetValidator

-- If we do not import Ledger, then
-- how to replace Ledger.Validator?

writeValidator :: FilePath -> Plutus.V2.Ledger.Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Plutus.V2.Ledger.Api.unValidatorScript

writePPBLFaucetScript :: IO (Either (FileError ()) ())
writePPBLFaucetScript = writeValidator "output/ppbl-faucet-plutus-v2.plutus" $ PPBLFaucetValidator.validator $ PPBLFaucetValidator.FaucetParams {
    PPBLFaucetValidator.accessTokenSymbol   = "2021db9a8aa0c4fde51ce244b5ad8628a3045787eaf6fadc52f31ee9"
  , PPBLFaucetValidator.accessTokenName     = "PPBLSummer2022"
  , PPBLFaucetValidator.faucetTokenSymbol   = "381f0068861bb8916e28732215a981c5d14124445c5f2cc2b3a1ba22"
  , PPBLFaucetValidator.faucetTokenName     = "tgimbal"
  , PPBLFaucetValidator.withdrawalAmount    = 700
}