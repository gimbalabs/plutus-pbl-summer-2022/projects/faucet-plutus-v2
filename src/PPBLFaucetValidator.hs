{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}


module PPBLFaucetValidator (FaucetParams (..), validator) where

-- are all of these necessary?
import              Plutus.V1.Ledger.Value
import              Plutus.V2.Ledger.Api
import              Plutus.V2.Ledger.Contexts
import              Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import              Plutus.Script.Utils.V2.Typed.Scripts (ValidatorTypes, TypedValidator, mkTypedValidator, mkTypedValidatorParam, validatorScript, mkUntypedValidator)
import              PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..))
import qualified    Prelude                   as Pr

data FaucetParams = FaucetParams
  { accessTokenSymbol   :: !CurrencySymbol
  , accessTokenName     :: !TokenName
  , faucetTokenSymbol   :: !CurrencySymbol
  , faucetTokenName     :: !TokenName
  , withdrawalAmount    :: !Integer
  }

PlutusTx.makeLift ''FaucetParams

newtype FaucetRedeemer = FaucetRedeemer {senderPkh :: PubKeyHash}

PlutusTx.unstableMakeIsData ''FaucetRedeemer
PlutusTx.makeLift ''FaucetRedeemer

{-# INLINEABLE faucetValidator #-}
faucetValidator :: FaucetParams -> Integer -> FaucetRedeemer -> ScriptContext -> Bool
faucetValidator faucet _ receiver ctx = traceIfFalse "Input needs PPBLSummer2022 token"           inputHasAccessToken &&
                                        traceIfFalse "PPBLSummer2022 token must return to sender" outputHasAccessToken &&
                                        traceIfFalse "Faucet token must be distributed to sender" outputHasFaucetToken &&
                                        traceIfFalse "Must return remaining tokens to contract"   faucetContractGetsRemainingTokens
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    receiverPkh :: PubKeyHash
    receiverPkh = senderPkh receiver

    allTokens :: [CurrencySymbol]
    allTokens = symbols $ valueSpent info

    inputHasAccessToken :: Bool
    inputHasAccessToken = (accessTokenSymbol faucet) `elem` allTokens

    valueToReceiver :: Value
    valueToReceiver = valuePaidTo info receiverPkh

    accessTokensToReceiver :: Integer
    accessTokensToReceiver = valueOf valueToReceiver (accessTokenSymbol faucet) (accessTokenName faucet)

    faucetTokensToReceiver :: Integer
    faucetTokensToReceiver = valueOf valueToReceiver (faucetTokenSymbol faucet) (faucetTokenName faucet)

    outputHasAccessToken :: Bool
    outputHasAccessToken = accessTokensToReceiver >= 1

    outputHasFaucetToken :: Bool
    outputHasFaucetToken = faucetTokensToReceiver >= withdrawalAmount faucet

    -- The UTXO input from Faucet
    ownInput :: TxOut
    ownInput = case findOwnInput ctx of
        Nothing -> traceError "faucet input missing"
        Just i  -> txInInfoResolved i

    -- The UTXO output back to Faucet
    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o -- There must be exactly ONE output UTXO
        _   -> traceError "expected exactly one faucet output"

    faucetInputValue :: Value
    faucetInputValue = txOutValue ownInput

    faucetOutputValue :: Value
    faucetOutputValue = txOutValue ownOutput

    faucetTokensFromContract :: Integer
    faucetTokensFromContract = valueOf faucetInputValue (faucetTokenSymbol faucet) (faucetTokenName faucet)

    faucetTokensToContract :: Integer
    faucetTokensToContract = valueOf faucetOutputValue (faucetTokenSymbol faucet) (faucetTokenName faucet)

    faucetContractGetsRemainingTokens :: Bool
    faucetContractGetsRemainingTokens = faucetTokensToContract >= faucetTokensFromContract - (withdrawalAmount faucet)

data FaucetTypes

instance ValidatorTypes FaucetTypes where
    type instance DatumType FaucetTypes = Integer
    type instance RedeemerType FaucetTypes = FaucetRedeemer

typedValidator :: FaucetParams -> TypedValidator FaucetTypes
typedValidator faucet = go faucet where
    go = mkTypedValidatorParam @FaucetTypes
        $$(PlutusTx.compile [|| faucetValidator ||])
        $$(PlutusTx.compile [|| wrap ||])
    wrap = mkUntypedValidator

validator :: FaucetParams -> Validator
validator = validatorScript . typedValidator